import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class HladaniePokladu {

	public static void main(String[] args) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		// vytvori ostrovy
		Ostrov Start = VytvaracOstrovov.vyvtorOstrovy();
		// vypise uvodny text a vypyta mena a uvita hraca
		Texty.vypisInfo();
		String meno = reader.readLine();
		Texty.pytajNick();
		String nick = reader.readLine();
		Texty.vitaj(nick);

		Hrac h = new Hrac(meno, nick);
		h.setOstrov(Start);

		System.out.println(" ----------- ");

		while (true) {
			Ostrov somNaOstrove = h.getOstrov();

			if (somNaOstrove.isSomKoniec()) {
				Texty.vyhra(h.getNick());
				break;
			}
			if (!somNaOstrove.daSaIstDalej()) {
				System.out.println("Lutujeme, z tohto ostrovu nie je cesty spat."
						+ " Pri pokuse o unik z tohto ostrova vas zozrali zraloky..");
				Texty.prehra();
				break;
			}

			somNaOstrove.vypisSebaAMoznosti();
			String dalej = reader.readLine();

			System.out.println(" ----------- ");
			
			if(!Test.jeDobryStringOver(dalej)) {
				Texty.prehra();
				break;
			}

			Integer chcemIst = Integer.parseInt(dalej);
			Ostrov dalsiOstrov = somNaOstrove.prechodNaOstrov(chcemIst);

			if (dalsiOstrov == null) {
				System.out.println("Zle id ostrovu. Toto id v ponuke nebolo. Koncim pre podozrenie z podvadzania");
				Texty.prehra();
				break;
			}

			h.setOstrov(dalsiOstrov);
		}

	}
}
