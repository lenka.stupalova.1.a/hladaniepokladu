
/**
 * @author CGI Trieda obsahuje udaje o hracovi, obsahuje jeho meno, nick, ostrov
 *         na ktorom sa nachadza a id ostrova na ktorom sa nachadza
 */
public class Hrac {

	private String meno;
	private String nick;

	private int somNaPolicku;
	private Ostrov ostrov;

	public Hrac(String meno, String nick) {
		this.meno = meno;
		this.nick = nick;
		this.somNaPolicku = 0;
	}

	public String getMeno() {
		return meno;
	}

	public void setMeno(String meno) {
		this.meno = meno;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public int getSomNaPolicku() {
		return somNaPolicku;
	}

	public void setSomNaPolicku(int somNaPolicku) {
		this.somNaPolicku = somNaPolicku;
	}

	public Ostrov getOstrov() {
		return ostrov;
	}

	public void setOstrov(Ostrov ostrov) {
		this.ostrov = ostrov;
	}

}
