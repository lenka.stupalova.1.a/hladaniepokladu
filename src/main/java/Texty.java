
/**
 * @author CGI
 * Tato trieda obsahuje rozne textove retazce, ktore sa vypisuju.
 */
public class Texty {

	/**
	 * Method writing out welcoming mssg and initial information
	 */
	public static void vypisInfo() {
		System.out.println("Vitajte v hre Hladnie Pokladu!");
		System.out.println("V tejto hre sa posuvate po polickach / ostrovoch. "
				+ "Cielom hry je dostat sa k ostrovu s pokladom.");
		System.out.println("Zadajte meno:");
		
	}

	/**
	 * This method writing out player's name and instructions to play
	 * @param meno
	 */
	public static void vitaj(String meno) {

        System.out.println("Vitaj na ceste za pokladom, " + meno);

        System.out.println("Posuvat sa mozte pomocou cisiel / id ostrovu.");
        
        System.out.println("Ak chcete hru ukoncit, napiste q alebo Koniec");
		
		System.out.println("Nachadzate sa na startovom policku.");	
		
	}

	public static void pytajNick() {
		System.out.println("Zadajte nick:");
		
	}
	
	

	public static void vyhra(String nick) {
		System.out.println("*** Gratulujeme! ***" );
		System.out.println("" + nick
				+ ", nasli ste poklad!" );
		System.out.println("  ,_-~~~-,    _-~~-_\r\n"
				+ " /        ^-_/      \\_    _-~-.\r\n"
				+ "|      /\\  ,          `-_/     \\\r\n"
				+ "|   /~^\\ '/  /~\\  /~\\   / \\_    \\\r\n"
				+ " \\_/    }/  /        \\  \\ ,_\\    }\r\n"
				+ "        Y  /  /~  /~  |  Y   \\   |\r\n"
				+ "       /   | {Q) {Q)  |  |    \\_/\r\n"
				+ "       |   \\  _===_  /   |\r\n"
				+ "       /  >--{     }--<  \\\r\n"
				+ "     /~       \\_._/       ~\\\r\n"
				+ "    /    *  *   Y    *      \\\r\n"
				+ "    |      * .: | :.*  *    |\r\n"
				+ "    \\    )--__==#==__--     /\r\n"
				+ "     \\_      \\  \\  \\      ,/\r\n"
				+ "       '~_    | |  }   ,~'\r\n"
				+ "          \\   {___/   /\r\n"
				+ "           \\   ~~~   /\r\n"
				+ "           /\\._._._./\\\r\n"
				+ "          {    ^^^    }\r\n"
				+ "           ~-_______-~\r\n"
				+ "            /       \\");
		
	}

	public static void prehra() {
		System.out.println("Prehrali ste. Skuste hru znovu.");
		
	}

}
